import React,{Component} from 'react'
import {View,Text} from 'react-native'
import DashBoardComponent from './myAccountComponent'
import makeVilla from './createVilla'
import EditPro from './EditProfile'
import VillaRules from './villaRules'
import { StackNavigator,NavigationActions } from 'react-navigation'


class MyAccount extends Component{

    render(){
        return (
            <DashBoardComponent navigation={this.props.navigation}/>
        );
    }
}






const SimpleApp = StackNavigator({
    Home: { screen: MyAccount},
    makeVilla: { screen: makeVilla},
    EditUserProfile:{screen:EditPro},
    villaRules:{screen:VillaRules}
});



export  default class MainComp extends Component{
    render(){
        return(

            <SimpleApp/>

        );
    }
}