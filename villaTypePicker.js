import ModalSelector from 'react-native-modal-selector'
import React,{Component} from 'react'
import {TextInput,View} from 'react-native'

export default class SampleApp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            textInputValue: ''
        }
    }





    render() {
        let index = 0;
        const data = [
            { key: index++, section: true, label: 'نوع ملک' },
            { key: index++, label: 'ویلا' },
            { key: index++, label: 'آپارتمان' }

        ];

        return (
            <View style={{flex:1, justifyContent:'space-around', paddingBottom:10,}}>


                <ModalSelector

                    data={data}
                    initValue="نوع ملک را مشخص کنید"
                    onChange={(option)=>{ this.setState({textInputValue:option.label})}}
                    selectTextStyle={
                        {fontFamily:'B Yekan',padding:5}
                    }

                />


            </View>
        );
    }
}