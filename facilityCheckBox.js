import React,{Component} from 'react'
import {View,Text,FlatList,Button} from 'react-native'
import { CheckBox } from 'react-native-elements'
import axios from 'axios'




class CheckBoxMaking extends Component{

    constructor(props){
        super(props);
        this.state={
            checked:false
        }
    }
    render(){
        return(

            <CheckBox
                right
                containerStyle={{backgroundColor:'white'}}
                title={this.props.facilitiesName}
                checked={ this.state.checked }
                onPress={   ()=>{
                    this.setState({
                        checked:!this.state.checked
                    })

                    if(this.state.checked==true){
                        console.log(1)
                    }
                }}
            />

        );
    }

}



export default class CheckFacility extends Component{
    constructor(props){
        super(props);
        this.state={
            facilities:[]
        }
    }

    componentDidMount(){
        let baseUrl=`http://vilayab.meandme.ir:80/facilities`;
        axios.get(baseUrl)
            .then(response=>{
                console.log(response.data);
                this.setState({
                    facilities:response.data
                })


            })
    }

    render(){
        return(

            <View>

            <FlatList
                data={this.state.facilities}

                renderItem={({ item,index}) => (
                    <CheckBoxMaking facilitiesName={item.name} />
                )}
                keyExtractor={(item, index) => item.idSpecialFacilities}
            />

</View>
        );
    }
}