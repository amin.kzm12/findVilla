import React, { Component } from 'react';
import {
    TouchableHighlight,
    StyleSheet,
    Text,
    ScrollView,
    View,
    Button,
    Image,

} from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons'
import Ic from 'react-native-vector-icons/MaterialIcons'

import {
    Dialog,
    ProgressDialog,
    ConfirmDialog,
} from 'react-native-simple-dialogs'

import FCP from './facilityCheckBox'



export default class App extends Component {

    constructor(props){
        super(props);
        this.state={
            checked:false
        }
    }


    state = {}

    openDialog(show) {
        this.setState({ showDialog: show })
    }

    openConfirm(show) {
        this.setState({ showConfirm: show })
    }

    openProgress() {
        this.setState({ showProgress: true })

        setTimeout(
            () => this.setState({ showProgress: false }),
            4000
        );
    }

    optionYes() {
        this.openConfirm(false);
        // Yes, this is a workaround :(
        // Why? See this https://github.com/facebook/react-native/issues/10471
        setTimeout(() => alert("Yes touched!"), 100);
    }

    optionNo() {
        this.openConfirm(false);
        // Yes, this is a workaround :(
        // Why? See this https://github.com/facebook/react-native/issues/10471
        setTimeout(() => alert("No touched!"), 100);
    }

    render() {
        return (
            <View style={styles.container}>

                <TouchableHighlight underlayColor="white"  onPress={() => this.openDialog(true)} >
                <View style={[styles.facilityField]}>


                    <View style={[styles.facilityIcon]}>
                        <Ic  name="pool" size={28}/>
                    </View>

                    <View style={[styles.facilityButton]}>
                        <Text style={{fontFamily:'B Yekan',fontSize:20}} >امکانات</Text>
                    </View>

                </View>
                </TouchableHighlight>

                <Dialog


                    visible={this.state.showDialog}
                    title="امکانات ویلا"
                    onTouchOutside={() => this.openDialog(false)}
                    contentStyle={{ justifyContent: 'center', alignItems: 'center' }}
                    animationType="fade">


                    <Icon size={28} name="close" onPress={() => this.openDialog(false)}/>

                    {/*<Button onPress={() => this.openDialog(false)} style={{ marginTop: 10,color:'black' }} title="CLOSE" />*/}
                    <FCP />


                </Dialog>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 20,
    },
    facilityField:{
         borderWidth: 1, height: 40,
        borderColor:'#bdbdbd',
        marginTop:20,
        borderRadius:5,
        flexDirection:'row',

        // justifyContent: 'center',


        // alignItems: 'center',

    }
    ,
    facilityButton:{

        color:'#515151',
        alignItems: 'flex-end',
        flex:10,
        marginTop:10,
        marginRight:10

    },

    facilityIcon:{
        alignItems:'flex-start',
        flex:1,
        marginTop:5,
        marginLeft:10

    }
});