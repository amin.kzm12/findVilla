import React,{Component} from 'react'
import {View,Text,StyleSheet,ScrollView,FlatList} from 'react-native'
import { Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Rating } from 'react-native-ratings';

import { Button,Container } from 'native-base';
import TablePrice from './tableOfPrice'
import Moment from 'moment';



export default class VillaDetail extends Component{
    constructor(props){
        super(props);

        this.state = {
            starCount: 3.5
        };
    }


    render(){
        const {params} = this.props.navigation.state;
        Moment.locale('en');



        return(
            <View>
                <View style={[styles.idVillaStyle]}>
                    <Text style={[styles.idVillaText]}>

    کد اقامتگاه:{params.villa.idVilla}
                    </Text>
                </View>
                <View style={[styles.borderBottomStyles]}></View>

                <View style={[styles.areaField]}>
                    <Text style={[styles.areaDetailText]}>متراژ ملک:{params.villa.area}متر</Text>
                    <Icon size={28} name="home" style={{paddingLeft:5}} />
                </View>


                <View style={[styles.areaField]}>
                    <Text style={[styles.areaDetailText]}>تعداد اتاق ها:۳</Text>
                    <Icon size={28} name="columns"  style={{paddingLeft:5}}/>
                </View>
                <View style={[styles.borderBottomStyles]}></View>


                <View style={[styles.areaField]}>
                    <Text style={[styles.areaDetailText,{paddingTop:15,fontSize:15,paddingRight:5}]}>{`${params.villa.user.firstname} ${params.villa.user.lastname}`}</Text>
                    <Avatar
                        medium
                        rounded
                        source={{uri: "/Users/aminkazemi/Desktop/amin.jpg"}}
                        onPress={() => console.log("Works!")}
                        activeOpacity={0.7}
                    />
                </View>



                <View style={[styles.areaField]}>
                    <Rating
                        type='star'
                        ratingCount={5}
                        imageSize={30}

                        onFinishRating={this.ratingCompleted}
                    />

                    <Text style={[styles.areaDetailText,{paddingTop:10,fontSize:15,paddingRight:5}]}>امتیاز</Text>
                </View>


                <View style={[styles.areaField]}>


                <View>
                    <FlatList
                        horizontal={true}
                        style={{alignSelf:'flex-start',paddingTop:5}}
                        data={params.villa.facilities}
                        renderItem={({ item,index}) => (


                            <Text style={{flexDirection:'row',
                            paddingRight:1,
                            paddingLeft:1}}>{`${item.name}`} {`,`}</Text>
                        )}
                        keyExtractor={(item) => item.idVilla}
                    />
                </View>
                    <Text style={[styles.areaDetailText,{paddingTop:10,fontSize:15,paddingRight:5}]}>امکانات:</Text>
                </View>


                <View style={[styles.borderBottomStyles]}></View>

                <View>
                    <FlatList

                        data={params.villa.villas}
                        renderItem={({ item,index}) => (
                            <TablePrice priceVilla={`${item.price} `} beginDate={`${Moment(item.availableStartTime).format('YYYY/MM/d')    }`}
                                        endDate={`${Moment(item.availableEndTime).format('YYYY/MM/d')    }`}
                            />
                        )}
                        keyExtractor={(item) => item.idVilla}
                    />
                </View>



                    <Button block style={[{marginTop:30,marginRight:30,marginLeft:30,backgroundColor:'#663096'}]}>
                        <Text style={[{fontFamily:'B Yekan',color:'white',fontSize:18}]}>رزرو</Text>
                    </Button>


            </View>

        );
    }
}



const styles=StyleSheet.create({

        idVillaStyle:{
            marginTop:40,
            alignItems:'center',
        },
        borderBottomStyles:{
            borderTopWidth:1.5,
            borderTopColor:'orange',
            marginRight:20,
            marginLeft:20,
        },
        idVillaText:{
            fontFamily:'B Yekan',
            fontSize:17,
            paddingBottom:15
        },
        areaField:{
            marginTop:15,
            marginRight:20,
            flex:0,
            paddingBottom:10,
            flexDirection:'row',
            alignSelf: 'flex-end',
        },
        areaDetailText:{
            fontFamily:'B Yekan',
            fontSize:15,
            paddingTop:10,
            alignSelf:'flex-end'

        },
        rectangle:{
            borderTopWidth:1.5,
            marginTop:30,
            marginBottom:30,
            marginRight:20,
            marginLeft:20,
            height:60,
            borderBottomWidth:1.5,
            borderLeftWidth:1.5,
            borderRightWidth:1.5,
            borderColor:'black'

        },
        firstRectangle:{
            height:30,
            backgroundColor:'white'
        }
    ,
        innerRectangle:{
            height:30,
            borderBottomWidth:1.5,
            backgroundColor:'#DCDCDC',
            alignItems: 'center',
            justifyContent:'center'
        },

    reserveButton:{
        alignItems: 'center',
        justifyContent:'center',

        alignSelf: 'center',
    },
    reserveButtonStyle:{

            backgroundColor:'#663096'
    }







});
