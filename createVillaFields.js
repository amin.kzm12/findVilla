import React,{Component} from 'react'
import {View,TextInput,StyleSheet,Text,TouchableHighlight} from 'react-native'
import ModalSelector from 'react-native-modal-selector'
import axios from 'axios'
import IPM from './IMP'
import CP from './cityPicker'
import Tst from './modalFacility'






var value;


 class SampleApp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            textInputValue: ''
        }
    }


    returnTextInput(){
        value=this.state.textInputValue;

    }





    render() {

        let index = 0;
        const data = [
            { key: index++, section: true, label: 'نوع ملک' },
            { key: index++, label: 'ویلا' },
            { key: index++, label: 'آپارتمان' }

        ];



        return (
            <View style={{flex:1, justifyContent:'space-around', paddingBottom:10,}}>
                <ModalSelector
                    data={data}
                    initValue="نوع ملک را مشخص کنید"
                    onChange={(option)=>{ this.setState({textInputValue:option.key})
                        this.props.updateState(option.key);
                    }}
                    selectTextStyle={
                        {fontFamily:'B Yekan',padding:5}
                    }
                />


            </View>
        );
    }
}














export default class createVillaComponents extends Component{

    constructor(props){
        super(props);

        this.state={
            area:0,
            rentCondition:'',
            idCity:0,
            idState:0,
            villaType:'',
            postalCode:0,
            numOfRoom:0,
            address:'',
            facilities:'',
            picture:undefined
        }
    }


    submit = () =>{

        const {area,rentCondition,idCity,idState,villaType,postalCode,numOfRoom,facilities,picture} = this.state;

            let villaProps={villaType:'',area:0,idCity:0,idState:0,rentCondition:'',postalCode:0,numOfRoom:0,facilities:'',picture:undefined};
            villaProps.area=area;
            villaProps.rentCondition=rentCondition;
            villaProps.idCity=idCity;
            villaProps.idState=idState;
            villaProps.villaType=villaType;
            villaProps.postalCode=postalCode;
            villaProps.numOfRoom=numOfRoom;
            // villaProps.address=address;
            villaProps.facilities=facilities;
            villaProps.picture=picture;
            console.log(villaProps,value);



            // axios.post('https://vue-js-http-request.firebaseio.com/data.json',villaProps)
            //     .then(function (response) {
            //     console.log(response);
            // })
            //     .catch(function (error) {
            //         console.log(error);
            //     });
        const data = new FormData();
        data.append('name', 'testName'); // you can append anyone.
        data.append('photo', {
            uri: villaProps.picture,
            type: 'image/jpeg', // or photo.type
            name: 'testPhotoName'
        });
        fetch('https://vue-js-http-request.firebaseio.com/', {
            method: 'post',
            body: data
        }).then(res => {
            console.log(res)
        });




    };


    updateState = (vm) => {
        this.setState({
            villaType: vm
        });
    };


    setIdState  = (vm)   => {
        this.setState({
            idState :   vm
        });
    };

    setIdCity   =  (vm)  => {
        this.setState({
            idCity:vm
        });
    };

    setUriImage  =   (vm)  =>   {
      this.setState({
          picture:vm
      });
    };

    setIdFacilities =  (vm) =>  {
        this.setState({
            facilities:vm
        })
    }











    render(){
        return(
            <View style={[styles.wholeView]}>

                <SampleApp updateState={this.updateState} />

                <Text style={styles.label} >مساحت</Text>

                    <TextInput
                        style={[styles.styleTextInput]}
                        onChangeText={(text) => {this.setState({area:text})}
                        }

                    />

                <Text style={styles.label} > شهر</Text>

                <CP setIdState={this.setIdState} setIdCity={this.setIdCity}/>


                <Text style={styles.label} >وضعیت اجاره</Text>

                <TextInput
                    style={[styles.styleTextInput]}
                    onChangeText={(text) => {this.setState({rentCondition:text})}
                    }
                />

                <Text style={styles.label} >کد پستی</Text>

                <TextInput
                    style={[styles.styleTextInput]}
                    onChangeText={(text) => {this.setState({postalCode:text})}
                    }
                />

                <Text style={styles.label} >تعداد اتاق</Text>

                <TextInput
                    style={[styles.styleTextInput]}
                    onChangeText={(text) => {this.setState({numOfRoom:text})}
                    }
                />






            <Tst/>


                <IPM setUriImage={this.setUriImage}/>


                <View style={{paddingBottom:15}}>
                    <TouchableHighlight style={styles.button} onPress={this.submit} underlayColor='#99d9f4'>
                        <Text style={styles.buttonText}>ثبت</Text>
                    </TouchableHighlight>
                </View>





            </View>
        );
    }
}














const styles=StyleSheet.create({
    wholeView:{
      marginTop:20,
        flex:1,
    },
    viewTextInput:{
        flexDirection:'row'
    },
    label:{
        alignSelf:'flex-end',
        fontFamily:'B Yekan',
        paddingTop:10,
        paddingBottom:10,
        fontSize:18

    },
    styleTextInput:{
        height:40,borderWidth:1,borderColor:'#d4d4d4',borderRadius:5,fontFamily:'B Yekan'
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center',
        fontFamily:'B Yekan'
    },
    button: {
        height: 36,
        backgroundColor: '#da3b75',
        borderColor: '#da3b75',
        borderWidth: 1,
        borderRadius: 8,
        marginTop:20,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    }






})


