import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,Button,Text,TextInput } from 'react-native';
import { NavigationBar,Title } from '@shoutem/ui'
import Picking from './villaTypePicker'
import VillaFields from './createVillaFields'


export default class App extends Component {

    handleSubmit = () => {
        const value = this._form.getValue(); // use that ref to get the form value
        console.log('value: ', value);
    }



    render() {
        return (


            <ScrollView style={styles.container}>

                <VillaFields/>
            </ScrollView>


        );
    }
}






const styles = StyleSheet.create({
    container: {

        // marginTop: 50,

        padding: 20,
        backgroundColor: '#ffffff',
    },

    title: {
        fontSize: 30,
        alignSelf: 'center',
        marginBottom: 30
    },

});

