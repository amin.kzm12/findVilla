import React, { Component } from 'react'
import { TabNavigator,TabBarBottom } from 'react-navigation'
import Icon from 'react-native-vector-icons/MaterialIcons'
import HomeComponent from './HomeComponent';
import Form from './form'
import Bill from './bill'
import Tests from './myAccount'
import Homes from './Home'

/**
 * Screen for first tab.
 * You usually will have this in a separate file.
 */

class Test extends Component {
    static navigationOptions = {
        tabBarLabel: " حساب من",
        tabBarIcon: () => <Icon size={28} name="account-box" color="black" />
    }

    render() {
        return <Tests/>
    }
}

class ContactUs extends Component {
    static navigationOptions = {
        tabBarLabel: "تماس با ما",
        tabBarIcon: () => <Icon size={28} name="phone" color="black" />
    }

    render() {
        return <HomeComponent/>
    }
}


class Requests extends Component {
    static navigationOptions = {
        tabBarLabel: "درخواست ها",
        tabBarIcon: () => <Icon size={28} name="edit" color="black" />
    }

    render() {
        return <Form/>
    }
}

/**
 * Screen for third tab.
 * You usually will have this in a separate file.
 *
 *
 *
 *
 *
 */

class Bills extends Component {
    static navigationOptions = {
        tabBarLabel: " صورتحساب",
        tabBarIcon: () => <Icon size={28} name="attach-money" color="black" />

    }

    render() {
        return <Bill/>
    }
}




class Home extends Component {
    static navigationOptions = {
        tabBarLabel: "خانه",
        tabBarIcon: () => <Icon size={28} name="home" color="black" />
    }

    render() {
        return <Homes/>
    }
}

/**
 * react-navigation's TabNavigator.
 */
const MyApp = TabNavigator({
    Test:{screen:Test},
    ContactUs: { screen: ContactUs },
    Requests: { screen: Requests },
    Bills:{screen:Bills},
    Home: { screen: Home },

}, {
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    tabBarOptions: {
        activeTintColor: '#33B1FF',
        tabBarIcon:{
            tintColor: '#33B1FF'
        },
        labelStyle: {
            fontSize: 13,
            fontWeight:'bold',
            fontFamily: 'B Yekan'
        },
        bottomNavigationOptions: {
            labelColor: 'red',
            rippleColor: 'red',
            tabs: {
                MoviesAndTV: {
                    barBackgroundColor: '#37474F'
                },
                Music: {
                    barBackgroundColor: '#00796B'
                },
                Books: {
                    barBackgroundColor: '#a1b2c3'
                },

                Money: {
                    barBackgroundColor: '#b1002a'
                }
            }
        }
    }
})






export default MyApp;
// AppRegistry.registerComponent('bottom', () => MyApp)
