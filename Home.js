import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator,AlertIOS,Image,StatusBar } from "react-native";
import { List, ListItem, SearchBar } from "react-native-elements";
import { Container, Header, Content, Card, CardItem, Thumbnail,  Button, Icon, Left, Body, Right } from 'native-base';
import Cards from './card'
import NewPages from './New'

import { StackNavigator,NavigationActions } from 'react-navigation'

class FlatListDemo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            data: [],

            // page: 1,
            // seed: 1,
            error: null,
            refreshing: false
        };
    }

    componentDidMount() {
        this.makeRemoteRequest();
    }

    makeRemoteRequest = () => {
        const { page, seed } = this.state;
        const url = `http://vilayab.meandme.ir:80/villa/all?pagenum=1&pagesize=10`;
        this.setState({ loading: true });

        fetch(url)
            .then(res => res.json())


            .then(res => {
                // if (page === 1) {
                this.setState({
                    data: res,
                    error: res.error || null,
                    loading: false,
                    refreshing: false
                });
            })


            .catch(error => {
                this.setState({ error, loading: false });
            });
    };

    handleRefresh = () => {
        this.setState(
            {

                refreshing: true
            },
            () => {
                this.makeRemoteRequest();
            }
        );

    };

    handleLoadMore = () => {
        this.setState(
            {
                page: this.state.page + 1
            },
            () => {
                this.makeRemoteRequest();
            }
        );
    };

    renderSeparator = () => {
        return (
            <View/>
        );
    };

    renderHeader = () => {
        return <SearchBar placeholder="Type Here..." lightTheme round />;

    };

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View>
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {

        return (
                <FlatList
                    style={{backgroundColor:'#fff'}}

                    // style={{marginTop:50}}
                    data={this.state.data}

                    renderItem={({ item,index}) => (
                        <Cards navigation={this.props.navigation} VillaWholeObject={item} name={`${item.user.firstname} ${item.user.lastname}`} city={`${item.city.name} `}  image = {(item.images!=undefined && item.images.length>0)?("http://vilayab.meandme.ir/static/uploads/"+item.images[0].imageaddress):('https://i.ytimg.com/vi/NtQBijgmdKQ/hqdefault.jpg')}/>
                    )}
                    keyExtractor={(item, index) => item.idVilla}
                    // ItemSeparatorComponent={this.renderSeparator}
                    // ListHeaderComponent={this.renderHeader}
                    // ListFooterComponent={this.renderFooter}
                    // onRefresh={this.handleRefresh}
                    // refreshing={this.state.refreshing}
                    // onEndReached={this.handleLoadMore}
                    // onEndReachedThreshold={50}
                />

        );
    }
}



const SimpleApp = StackNavigator({
    Home: { screen: FlatListDemo},
    Chat: { screen: NewPages}
});



export  default class MainComp extends Component{
    render(){
        return(

            <SimpleApp/>

        );
    }
}
