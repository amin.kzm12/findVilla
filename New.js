import React,{Component} from 'react';
import {View,Text,ScrollView} from 'react-native';
import ImageSw from './ImageSwiper'
import VillaDetails from './VillaDetail'



export default class NewPage extends Component{

    render(){
        return(

                <ScrollView style={{flex:1,backgroundColor:'white'}}>
                    <View>
                        <ImageSw/>
                    </View>
                    <View>
                        <VillaDetails navigation={this.props.navigation}/>
                    </View>
                </ScrollView>
        );
    }
}
