import React,{Component} from 'react'
import {Text,View,Image,StyleSheet,Dimensions} from 'react-native'
import ImageSlider from 'react-native-image-slider';

export default class SwippingImage extends Component{
    constructor(props){
        super(props);
        this.state={
            position: 1,
            interval: null
        }
    }

    componentWillMount() {

        this.setState({
            interval: setInterval(() => {
                this.setState({position: this.state.position === 2 ? 0 : this.state.position + 1});
            }, 5000)
        })
    }

    componentWillUnmount() {
        clearInterval(this.state.interval);
    }




    render(){
        return(

            <View>


                <ImageSlider images={[
                    'http://www.costaprestige.com/images/cms/data/new_villa_1/CPOT-1/facade.jpg',
                    'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/12501548_870380973072691_86707852_n.jpg?ig_cache_key=MTIyMTUzNTA0NzUyNzkxODU3MA%3D%3D.2\'',
                    'http://www.sunestates.com/data/images/luxury-villas/sol-banyan-villas.jpg'
                ]}

                position={this.state.position}
                onPositionChanged={position => this.setState({position})}/>


            </View>
        );
    }
}