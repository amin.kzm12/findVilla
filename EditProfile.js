import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,Button,TouchableHighlight,Text } from 'react-native';
import { NavigationBar,Title } from '@shoutem/ui'





import t from 'tcomb-form-native';
const Form = t.form.Form;


const area='مساحت'

const User = t.struct({
    'نام ':t.String,
    'نام خانوادگی':t.String,
    'شماره تماس':t.Number,
    'شناسه کاربری':t.String,
    'رمز عبور':t.String,
    'آدرس':t.String
});






export default class App extends Component {

    handleSubmit = () => {
        const value = this._form.getValue(); // use that ref to get the form value
        console.log('value: ', value);
    }



    render() {
        return (


            <ScrollView style={styles.container}>




                <Form type={User}
                      ref={c => this._form = c}

                />

                <View style={{paddingBottom:30}}>
                    <TouchableHighlight style={styles.button} onPress={this.handleSubmit} underlayColor='#99d9f4'>
                        <Text style={styles.buttonText}>ثبت</Text>
                    </TouchableHighlight>
                </View>
            </ScrollView>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
        backgroundColor:'white'

    },

    title: {
        fontSize: 30,
        alignSelf: 'center',
        marginBottom: 30
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center',
        fontFamily:'B Yekan'

    },
    button: {
        height: 36,
        backgroundColor: '#da3b75',
        borderColor: '#da3b75',
        borderWidth: 1,
        borderRadius: 8,
        marginTop:10,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    }
});

