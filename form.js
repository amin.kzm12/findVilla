import React, { Component } from 'react';
import {StyleSheet,View,Text,TouchableHighlight} from 'react-native'
import { Container, Header, Content, Form, Item, Input, Label,Button,CheckBox } from 'native-base';
import { Divider,Slider } from 'react-native-elements';
import { NavigationBar,Title } from '@shoutem/ui'
import CityPicker from './cityPicker'
import Icon from 'react-native-vector-icons/MaterialIcons'


export default class form extends Component{

    constructor(props){
        super(props);
        this.state={value:0,numOfRooms:0,checked:false}
    }



    render(){
        return(
            <View>
                <NavigationBar
                    centerComponent={<Title>در خواست ها</Title>}
                />
                <View style={style.selectCity}>
                    <Text style={[style.selectCityText,style.persianFont]}>نام شهر مورد نظر خود را وارد کنید:</Text>
                    <CityPicker/>
                </View>



                <View style={style.setPriceField}>
                    <Text style={[style.setPriceTxt,style.persianFont]}>تعیین بازه قیمت</Text>

                </View>



                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center',marginTop:25}}>
                        <Slider
                            value={this.state.value}
                            onValueChange={(value) => this.setState({value})}
                            thumbTintColor={'purple'}
                            minimumTrackTintColor={'purple'}
                            minimumValue={10}
                            maximumValue={1000}
                            step={10}
                            trackStyle={{width:300}}


                        />
                        <Text style={style.persianFont}>Value: {this.state.value}</Text>
                    </View>


                <View style={style.numOfRoomField}>
                    <Text style={[style.numOfRoom,style.persianFont]}>تعداداتاق ها</Text>
                </View>

                <View style={style.setNumOfRoom}>

                    <View style={{flexDirection:'row'}}>


                        <TouchableHighlight onPress={()=>{
                            if(this.state.numOfRooms>0){
                                this.setState({numOfRooms:this.state.numOfRooms-1})
                            }


                        }}>

                        <Icon name="remove-circle-outline" size={28} />
                        </TouchableHighlight>



                        <Text style={[{padding:5},style.persianFont]}>{this.state.numOfRooms}</Text>


                        <TouchableHighlight onPress={()=>{

                            this.setState({numOfRooms:this.state.numOfRooms+1})

                        }}>
                        <Icon name="add-circle-outline" size={28} />

                        </TouchableHighlight>
                    </View>
                </View>


                <View>
                    <View style={style.accessoriesField}>
                        <Text style={style.neededAccessory}>
                                            امکانات ضروری
                        </Text>
                    </View>
                </View>



                <View  style={{alignItems:'flex-end',marginTop:25}}>
                    <View style={[{marginRight:20,flexDirection:'row'}]}>
                        <CheckBox style={{marginRight:20}}
                            onPress={
                                ()=>this.setState({checked:!this.state.checked})
                            }
                            checked={this.state.checked}
                        />

                        <Text style={style.persianFont}>
                            استخر
                        </Text>
                    </View>

                </View>




            </View>
        );
    }
}
const style=StyleSheet.create({

   selectCity:{
        flex:1,
       alignItems:'center',
       marginTop:100
   },

    selectCityText:{
       fontSize:18

    },
    setPriceField:{

        alignItems:'flex-end',

        marginTop:120,
        marginRight:10
    }

    ,
    setPriceTxt:{
        // alignSelf:'flex-end',
       fontSize:20,
        color:'purple'
    },
    numOfRoomField:{

        alignItems:'flex-end',

        marginTop:50,
        marginRight:10,


    },
    numOfRoom:{
        fontSize:20,
        fontWeight:'normal'
    },

    setNumOfRoom:{
        alignItems:'flex-start',
    },
    accessoriesField:{
       alignItems:'center',
        marginTop:20
    }
    ,
    neededAccessory:{
       fontFamily:'B Yekan',
        fontSize:20
    }
    ,
    persianFont:{
        fontFamily:'B Yekan'
    }



});

