import React, { Component } from 'react';
import { Image,View,ScrollView,StyleSheet,TouchableHighlight } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Left, Body, Right } from 'native-base';
import { Avatar } from 'react-native-elements';


export default class CardImageExample extends Component {

    render() {
        let url = typeof(this.props.image);
        const { navigate } = this.props.navigation;
        return (
            <View style={[styles.styleCard]}>
                <TouchableHighlight onPress={() => navigate('Chat',{villa:this.props.VillaWholeObject})} underlayColor="white">
                       <Content>
                        <Card >
                            <CardItem >
                                <Left>
                                    <Avatar
                                        medium
                                        rounded
                                        source={{uri: "/Users/aminkazemi/Desktop/amin.jpg"}}
                                        onPress={() => console.log("Works!")}
                                        activeOpacity={0.7}
                                    />

                                    <Right>

                                        <Text style={styles.txt}>{this.props.name}</Text>
                                        <Text note style={styles.txt}>{this.props.city}</Text>
                                    </Right>
                                </Left>
                            </CardItem>
                            <CardItem cardBody>
                                <Image source={{uri:this.props.image}}  style={{height: 200, width: null, flex: 1}}/>
                            </CardItem>
                            <CardItem>

                            </CardItem>
                        </Card>


                       </Content>
                </TouchableHighlight>
            </View>



        );
    }
}


const styles=StyleSheet.create({
    txt:{
        fontFamily:'B Yekan',
    },
    iconStyle:{
        color:'#00a7a8'
    },
    styleCard:{
        marginLeft:10,
        marginRight:10,

    }
})


