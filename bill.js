import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body,Button } from 'native-base';
import {View,ScrollView,StyleSheet} from 'react-native'
import { NavigationBar,Title } from '@shoutem/ui'

export default class CardHeaderFooterExample extends Component {
    render() {
        return (
            <Container>
                <Header />
                <Content>
                    <Card>

                        <CardItem header style={[{alignSelf: 'center'}]}>
                            <Text style={style.txt}>اطلاعات در خواست شما</Text>
                        </CardItem>

                        <CardItem style={style.cardItemStyle}>

                            <Body>
                                <View style={[style.rowField,{padding:20}]}>

                                    <Text style={[{flex:1},style.txt]}>

                                        ۱
                                    </Text>


                                    <Text style={[{flex:-1},style.txt]}>
                                            تعداد مهمان
                                    </Text>


                                </View>




                                <View style={[style.rowField,{padding:20}]}>

                                    <Text style={[{flex:1},style.txt]}>

                                        ۱۳۹۶/۲/۱۰
                                    </Text>


                                    <Text style={[{flex:-1},style.txt]}>
                تاریخ شروع
                                    </Text>


                                </View>



                                <View style={[style.rowField,{padding:20}]}>
                                    <Text style={[{flex:1},style.txt]}>
                                        ۱۴۰۰/۱۰/۲۰
                                    </Text>


                                    <Text style={[{flex:-1},style.txt]}>
                                                                تاریخ پایان
                                    </Text>


                                </View>



                                <View style={[style.rowField,{padding:20}]}>

                                    <Text style={[{flex:1},style.txt]}>
                                        ۱۰۰۰۰۰۰
                                    </Text>


                                    <Text style={[{flex:-1},style.txt]}>
                                         قیمت
                                    </Text>


                                </View>



                                <View style={[style.rowField,{padding:20}]}>

                                    <Text style={[{flex:1},style.txt]}>
پرداخت نشده
                                    </Text>


                                    <Text style={[{flex:-1},style.txt]}>
                                                                    وضعیت
                                    </Text>

                                </View>
                            </Body>

                        </CardItem>

                            <Button full light>
                                <Text style={style.buttonTxtColor}>پرداخت صورتحساب</Text>
                            </Button>

                    </Card>
                </Content>
            </Container>



        );
    }
}


const style=StyleSheet.create({
    rowField:{
        flex: 1, flexDirection: 'row',borderBottomColor: '#888888',
        borderBottomWidth: 1
    },


    buttonTxtColor:{
      color:'#7D4888',
        fontWeight:'bold',
        fontFamily:'B Yekan'


    },

    cardItemStyle:{
        alignSelf: 'center',borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#d6d7da'
    },

    txt:{
        fontFamily:'B Yekan'
    }





});
