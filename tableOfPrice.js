import React,{Component} from 'react'
import {Text,FlatList,View,StyleSheet} from 'react-native'


export default class Table extends  Component{
    render(){
        return(



            <View style={[styles.rectangle]}>

                <View style={[styles.firstRectangle]}>

                    <Text>
                          {this.props.beginDate}
                    </Text>
                    <Text style={styles.toStyle}>تا</Text>

                    <Text>{this.props.endDate}</Text>
                </View>
                <View style={[styles.innerRectangle]}>
                    <Text>
                        {this.props.priceVilla}
                        <Text style={[styles.toman]}>تومان</Text>
                    </Text>
                </View>
            </View>
        );
    }
}



const styles=StyleSheet.create({
    rectangle:{
        borderTopWidth:1.5,
        marginTop:30,
        marginBottom:30,
        marginRight:20,
        marginLeft:20,
        height:60,
        borderBottomWidth:1.5,
        borderLeftWidth:1.5,
        borderRightWidth:1.5,
        borderColor:'black'

    },
    firstRectangle:{
        flex:1,
        flexDirection:'row',
        height:30,
        backgroundColor:'white',
        alignItems: 'center',
            justifyContent:'center'
    }
    ,
    innerRectangle:{
        height:30,
        borderBottomWidth:1.5,
        backgroundColor:'#DCDCDC',
        alignItems: 'center',
        justifyContent:'center'
    },
    toman:{
        fontFamily:'B Yekan',
        fontWeight:'bold'
    },
    toStyle:{
        paddingLeft:5,
        paddingRight:5
    }



})