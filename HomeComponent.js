import React, { Component} from 'react';
import { ActivityIndicator, ListView, Text, View,AlertIOS,ScrollView,StyleSheet } from 'react-native';
import { NavigationBar,Title } from '@shoutem/ui'

import Icon from 'react-native-vector-icons/MaterialIcons'



export default class Movies extends Component {
    // constructor(props) {
    //     super(props);
    // }

    render() {
        return (
            <View style={{flex:1}}>
                <NavigationBar
                    centerComponent={<Title>تماس با ما</Title>}
                />
                <View style={style.aboutUs} >

                    <Text style={style.aboutUsText}>

                        لورم ایپسوم یا طرح‌نما (به انگلیسی: Lorem ipsum) به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد. معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از متن‌های آزمایشی و بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند که صفحه طراحی یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها چگونه در نظر گرفته شده‌است. از آنجایی که طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برند. </Text>

                </View>





                <View style={{flex: 1,paddingTop:20}}>
                    <View style={{ backgroundColor: 'whitesmoke',justifyContent:'flex-end',flexDirection:'row',padding:10}}>
                        <Text style={[style.phoneNumber]}>۰۹۱۷۶۰۶۲۱۳۵</Text>
                        <Icon name="phone" size={28} style={{paddingTop:5}}/>
                    </View>

                    <View style={{ backgroundColor: 'whitesmoke',justifyContent:'flex-end',flexDirection:'row',padding:10,marginTop:10}}>
                        <Text style={[style.phoneNumber]}>WWW</Text>
                        <Icon name="web" size={28}  style={{paddingTop:5}}/>
                    </View>

                    <View style={{ backgroundColor: 'whitesmoke',justifyContent:'flex-end',flexDirection:'row',padding:10,marginTop:10}}>
                        <Text style={[style.phoneNumber]}>Address</Text>
                        <Icon name="place" size={28}  style={{paddingTop:5}}/>
                    </View>

                </View>
            </View>
        );
    }
}


const style=StyleSheet.create({
   aboutUs:{
        marginTop:100,
       justifyContent:'flex-end',
       alignItems:'center',
       flexDirection:'row',
   },


   aboutUsText:{
       paddingLeft:10,
       paddingRight:3,
       fontFamily:'B Yekan',
       lineHeight: 20,
       color:'violet'
   },

    phoneNumber:{
       fontFamily:'B Yekan',
        fontWeight:'bold',
        padding:10,
        fontSize:17



    }






});
