import React,{Component} from 'react'
import {ScrollView,View,StyleSheet,TouchableHighlight} from 'react-native'
import { Avatar } from 'react-native-elements';
import { Container, Header, Content, Card, CardItem, Text, Icon, Right,Left } from 'native-base';




export default class dashboardAccount extends Component{
    render(){
        const { navigate } = this.props.navigation;
        return(
            <ScrollView style={{backgroundColor:'white'}}>
                <View style={[styles.AvatarDiv]}>
                    <Avatar
                        xlarge
                        rounded
                        source={{uri: "http://cdn.arn.com.au/media/12763/jamie-dornan.jpg"}}
                        onPress={() => console.log("Works!")}
                        activeOpacity={0.7}
                    />
                </View>


                <View style={[styles.cardItemMenu]}>
                    <Content>
                        <Card>

                            <TouchableHighlight onPress={() => navigate('makeVilla')}>
                                <CardItem >
                                    <Left>
                                        <Icon name="arrow-back" />
                                    </Left>
                                    <Right>
                                        <Text style={[styles.font]}>ایجاد ویلا</Text>
                                    </Right>
                                </CardItem>
                            </TouchableHighlight>



                            <TouchableHighlight onPress={() => navigate('EditUserProfile')}>
                                <CardItem>
                                    <Left>
                                        <Icon name="arrow-back" />
                                    </Left>
                                    <Right>
                                        <Text style={[styles.font]}> ویرایش اطلاعات کاربری</Text>
                                    </Right>
                                </CardItem>
                            </TouchableHighlight>


                            <TouchableHighlight onPress={() => navigate('villaRules')}>
                                <CardItem>
                                    <Left>
                                        <Icon name="arrow-back" />
                                    </Left>
                                    <Right>
                                        <Text style={[styles.font]}>قوانین ویلا یاب</Text>
                                    </Right>
                                </CardItem>
                            </TouchableHighlight>




                            <TouchableHighlight onPress={() => navigate('makeVilla')}>
                                <CardItem>
                                    <Left>
                                        <Icon name="arrow-back" />
                                    </Left>
                                    <Right>
                                        <Text style={[styles.font,{paddingBottom:5}]}> خروج</Text>
                                    </Right>
                                </CardItem>
                            </TouchableHighlight>
                        </Card>
                    </Content>
                </View>
            </ScrollView>
        );
    }
}


const styles=StyleSheet.create({
   AvatarDiv:{
       flex:1,
       alignItems:'center',
       marginTop:100
   },

    cardItemMenu:{
       marginTop:50,
        marginRight:10,
        marginLeft:10
    },

    font:{
       fontFamily:'B Yekan'
    }


});
